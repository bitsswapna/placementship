<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();

//     Route::post('login', 'AuthController@login');
// });

//Common Section//
    //==========================//
        //Qualification list API
        Route::get('qualifications', 'CommonController@getAllQualifications');
        //Course list API
        Route::get('courses', 'CommonController@getAllCourses');
        Route::get('locations', 'CommonController@getAllLocations');
        Route::get('no-of-partners', 'CommonController@getAllNumberOfPartners');


//Admin
Route::group([
    'prefix' => 'admin'
], function () {

    //Admin Login,Signup and Logout Section
    //login
    Route::post('login', 'AdminAuthController@login');
    Route::post('signup', 'AdminAuthController@signup');
    //logout
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', ['middleware'=>'check-permission:user|admin','uses'=>'AdminAuthController@logout']);
        Route::get('user',['middleware'=>'check-permission:user|admin','uses'=>'AdminAuthController@user']);
        Route::post('change-password',['middleware'=>'check-permission:user|admin','uses'=>'AdminAuthController@changePassword']);

    //Student or Employee section//
    //==========================//
        //student,employee and firm count api
        Route::get('counts', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@getCounts']);
        //student or employee list API
        Route::post('user-list', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@getAllemployees']);
        //student 0r employee single view details
        Route::get('view-user', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@getAuser']);
        //Student or employee edit API
        Route::post('edit-user', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@editBasicsUser']);
        //Student or employee Resume edit API
        Route::post('update-user-resume', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@updateResume']);
        //Student or employee delete API
        Route::get('delete-user', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@deleteUser']);
        //Student or employee View Applications API
        Route::get('view-user-applications', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@viewUserApplication']);
        //Student or employee View Shortlisted Applications API
        Route::get('view-user-shortlisted-applications', ['middleware'=>'check-permission:user|admin','uses'=>'AdminUserController@viewUserShortListedApplication']);

    //Location or Zone Section//
    //==========================//
        //Location list API
        Route::get('locations', ['middleware'=>'check-permission:user|admin','uses'=>'LocationController@getAllLocations']);
        //Location add API
        Route::post('location-add', ['middleware'=>'check-permission:user|admin','uses'=>'LocationController@addAllLocations']);
        //Location edit API
        Route::post('location-edit', ['middleware'=>'check-permission:user|admin','uses'=>'LocationController@editLocations']);
        //Location view API
        Route::get('location-view', ['middleware'=>'check-permission:user|admin','uses'=>'LocationController@viewLocations']);
        //Location delete API
        Route::post('delete-location', ['middleware'=>'check-permission:user|admin','uses'=>'LocationController@deleteLocations']);

    //Firm Section//
    //==========================//
        //Firm list API
        Route::post('firm-list', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@getAllFirms']);
        //Firm add API
        Route::post('add-firm', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@addFirms']);
        //Firm edit API
        Route::post('edit-firm', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@editFirms']);
        //Firm edit API
        Route::get('view-firm', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@viewFirm']);
        //Firm delete API
        Route::get('delete-firm', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@deleteFirm']);
        //Firm block API
        Route::get('block-firm', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@blockFirm']);
        //Firm view requirements API
        Route::get('view-firm-requirements', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@viewFirmRequirements']);

        //Firm add gallery API
        Route::post('add-firm-gallery', ['middleware'=>'check-permission:user|admin','uses'=>'FirmController@addFirmGallery']);


    //Firm Requirement Section//
    //==========================//
        //Firm Requirement list API
        Route::post('firm-requirements', ['middleware'=>'check-permission:user|admin','uses'=>'FirmRequirementController@getAllRequirements']);
        //Firm Approve/Expire a Requirement API
        Route::get('change-requirement-status', ['middleware'=>'check-permission:user|admin','uses'=>'FirmRequirementController@changeStatus']);
        //Requirement  Applications list API
        Route::post('requirement-applications', ['middleware'=>'check-permission:user|admin','uses'=>'FirmRequirementController@getAllApplications']);
        //Requirement  Application Assign to the firm
        Route::get('applications-assign', ['middleware'=>'check-permission:user|admin','uses'=>'FirmRequirementController@assignApplications']);


    //QUESTION ANSWER Section//
    //==========================//
        //Question list API
        Route::get('questions-list', ['middleware'=>'check-permission:user|admin','uses'=>'ExamController@getAllQuestions']);
        //Question add API
        Route::post('questions-add', ['middleware'=>'check-permission:user|admin','uses'=>'ExamController@addQuestions']);
        //Question edit API
        Route::post('questions-edit', ['middleware'=>'check-permission:user|admin','uses'=>'ExamController@editQuestions']);
        //Question delete API
        Route::post('questions-delete', ['middleware'=>'check-permission:user|admin','uses'=>'ExamController@deleteQuestions']);
         //Question view API
         Route::get('questions-view', ['middleware'=>'check-permission:user|admin','uses'=>'ExamController@viewQuestions']);
        //Exam Time add API
        Route::post('exam-time', ['middleware'=>'check-permission:user|admin','uses'=>'ExamController@addExamTime']);





    });
});

//Student/Employee

Route::group([
    'prefix' => 'user'
], function () {

    //Student/Employee Login,Signup and Logout Section
    //login and signup
    Route::post('login', 'UserAuthController@login');
    Route::post('forgot-password', 'UserAuthController@forgotPassword');
    Route::post('verify-forgot-password-otp', 'UserAuthController@verifyOtp');
    Route::post('change-password', 'UserAuthController@changePassword');
    Route::post('signup', 'UserAuthController@signup');
    Route::post('mobile-otp-verification', 'UserAuthController@mobileVerification');
    Route::post('mobile-otp-resend', 'UserAuthController@mobileOtpResend');
    Route::post('setup-password', 'UserAuthController@setUpPassword');


    Route::group([
        'middleware' => 'auth:api'
      ], function() {
        Route::group(['middleware'=>'check-permission:user|student|employee'], function () {
          //Sign Up MoRE Details
          Route::post('signup-details', 'UserAuthController@signupFullDetails');
          //Resuume Upload
          Route::post('user-resume-upload', 'UserAuthController@uploadResume');
          //logout
          Route::get('logout', 'UserAuthController@logout');
          //Exam Question list API
          Route::get('exam-questions-list', 'UserExamController@getAllQuestions');
          //Attend Exam
          Route::post('attend-exam', 'UserExamController@attendExam');
          //Profile view of user/student
          Route::get('user-profile-view', 'UserAuthController@getAuser');
          //Profile edit of user/student
          Route::post('user-profile-edit', 'UserAuthController@editBasicsUser');

           //All requirements based on role_id
          Route::post('all-requirements', 'UserApplicationController@getAllRequirements');

          //view  requirement details
          Route::get('view-requirement-details', 'UserApplicationController@viewDetailRequirements');

         //Apply For a Requirement
          Route::post('apply-requirement', 'UserApplicationController@applyRequirement');
        });

    });

});


//Firm

Route::group([
    'prefix' => 'firm'
], function () {


    //Firm Login,Signup and Logout Section
    //login and signup

    Route::post('login', 'FirmAuthController@login');
    Route::post('signup', 'FirmAuthController@signup');
    Route::post('mobile-otp-verification', 'FirmAuthController@mobileVerification');
    Route::post('mobile-otp-resend', 'FirmAuthController@mobileOtpResend');
    Route::post('setup-password', 'FirmAuthController@setUpPassword');
    Route::post('signup-details', 'FirmAuthController@signupFullDetails');
    Route::post('firm-gallery', 'FirmAuthController@firmgallery');

    Route::group([
        'middleware' => 'auth:api'
      ], function() {
        Route::group(['middleware'=>'check-permission:user|firm'], function () {
          //logout
          Route::get('logout', 'FirmAuthController@logout');

          //Profile view of firm
          Route::get('firm-profile-view', 'FirmAuthController@viewFirm');
          //Profile edit of firm
          Route::post('firm-profile-edit', 'FirmAuthController@editFirms');

           //All requirements
          Route::post('all-requirements', 'FirmDetailsController@getAllRequirements');

          //Post a Requirement
          Route::post('post-requirements', 'FirmDetailsController@postRequirements');

          //View Applications
          Route::post('view-requirement-applications', 'FirmDetailsController@getAllApplications');

          //view  requirement details
          Route::get('view-requirement-details', 'FirmDetailsController@viewDetailRequirements');

         //Apply For a Requirement
          Route::post('apply-requirement', 'FirmDetailsController@applyRequirement');
        });

    });

});

//

