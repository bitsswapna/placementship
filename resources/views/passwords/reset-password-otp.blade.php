{{--<div>--}}
{{--<p>Hello {{ $name }},</p>--}}
{{--We received a request to reset your Viegenomics password. Click the link below to choose a new one: <br>--}}
{{--<a style="    background-color: #44af84;--}}
{{--text-decoration: none;--}}
{{--color: #ffffff;--}}
{{--padding: 5px 12px;--}}
{{--border-radius: 4px;--}}
{{--margin-bottom: -8px;--}}
{{--cursor: pointer;position: relative;--}}
{{--text-transform: uppercase;--}}
{{--font-size: 15px;--}}
{{--display: inline-block;" href="{{url("reset/password/".$token)}}">Reset Your Password</a>--}}
{{--</div>--}}


        <!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Viegenomics</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

</head>

<body
        syle="font-family: 'Roboto', sans-serif;;background-color: #fff; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body"
       style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #fff;">
    <tr>
        <td style="font-family: 'Roboto', sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container"
            style="font-family: 'Roboto', sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
            <div class="content"
                 style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

                <!-- START CENTERED WHITE CONTAINER -->

                <table class="main"
                       style="width:100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;border:1px solid #f2f2f2;border-collapse: collapse;">
                    <tr>
                        <td style="background:#f5f2f2;padding:10px;-webkit-box-shadow: 0px 0px 13px 0px rgba(82,63,105,0.05);
              box-shadow: 0px 0px 13px 0px rgba(82,63,105,0.05);width:100%;">
                            <div>
                                <a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt="brand" style="max-width: 100%;width: 130px;"></a>
                            </div>
                        </td>
                    </tr>
                    <tr>


                    </tr>

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper"
                            style="font-family: 'Roboto', sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 0px;">
                            <table border="0" cellpadding="0" cellspacing="0"
                                   style="padding:20px;border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                <tr>
                                    <td style="font-family: 'Roboto', sans-serif; font-size: 14px; vertical-align: top;">
                                        <p
                                                style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                            Hi {{ $name }},
                                        </p>
                                        <p
                                                style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                            Greetings!
                                        </p>
                                        <p  style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> We received a request to reset your Placementship Login password. Below is your OTP to reset password:
                                        </p>
                                        <a style=" background-color: #44af84;text-decoration: none;color: #ffffff;padding: 2px 10px;border-radius: 2px;text-transform: uppercase;font-size: 15px;display: inline-block;margin-bottom: 15px;font-size: 14px;text-transform: none;" href="#">{{$token}}</a>
                                        <p
                                                style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                            Best Regards,
                                        </p>
                                        <p
                                                style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                            Team Placementship
                                        </p>

                                        <!--  -->

                                    </td>

                                </tr>

                            </table>

                            <table style="background:#f5f2f2;width: 100%;">
                                {{--<tr>--}}
                                    {{--<td style="padding: 5px;border-right:1px dotted #777;width: 50%;--}}
                    {{--padding-left: 20px;">--}}

                                        {{--<p--}}
                                                {{--style="font-family: 'Roboto', sans-serif;font-size: 15px; font-weight: 500; margin: 0; Margin-bottom: 15px;">--}}
                                            {{--What Next?</p>--}}

                                        {{--<p--}}
                                                {{--style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;">--}}
                                            {{--Track your orders at <a href="#!" style="color:#333">My Orders</a></p>--}}
                                    {{--</td>--}}
                                    {{--<td style="padding: 5px;width: 50%;padding-right: 20px;padding-left: 15px;">--}}

                                        {{--<p--}}
                                                {{--style="font-family: 'Roboto', sans-serif;font-size: 15px; font-weight: 500; margin: 0; Margin-bottom: 15px;">--}}
                                            {{--Any Questions?</p>--}}

                                        {{--<p--}}
                                                {{--style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 15px;">--}}
                                            {{--Get in touch with our 24x7 <a href="#!" style="color:#333">Customer Care </a>team.</p>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                <tr style="width: 100%">
                                    <td style="text-align:center;border-bottom:1px solid #e8e8e8;border-top:1px solid #e8e8e8" colspan="2">
                                        <p
                                                style="font-family: 'Roboto', sans-serif; font-size: 13px; font-weight: normal; margin: 0; Margin-bottom: 10px;margin-top:10px;">
                                            <a href="" style="text-decoration: none;color:#44af84">placementship.in</a></p>
                                    </td>
                                </tr>
                                {{--<tr>--}}
                                    {{--<td style="text-align:center;" colspan="2">--}}
                                        {{--<p--}}
                                                {{--style="font-family: 'Roboto', sans-serif; font-size: 11px; font-weight: normal; margin: 0; Margin-bottom: 5px;margin-top: 5px;">--}}
                                            {{--24x7 Customer Support | Buyer Protection | Flexible Payment Options </p>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}

                            </table>
                        </td>
                    </tr>

                    <tr>

                    </tr>

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
    </tr>
</table>
</body>

</html>
