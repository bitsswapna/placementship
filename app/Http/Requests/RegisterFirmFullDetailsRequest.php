<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFirmFullDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'id'=>'required',
            'location_zone'=>'required',
            'no_of_partners_id'=>'required',
            'website_id'=>'required',
            'stipened_offered'=>'required',
            'about_firm'=>'required',
            'exposure_offered'=>'required'
        ];
    }
}
