<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'type'=>'required',
            'sro_number'=>'required_if:type,2',
            'mobile'=>'required_if:type,1|regex:/[0-9]{9}/',
            'password'=>'required',
        ];
    }
}
