<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FirmUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'id'=>'required',
            'name'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'location_zone'=>'required',
            'no_of_partners_id'=>'required',
            'exposure_offered'=>'required',
            'about_firm'=>'required',
            'stipened_offered'=>'required',
            'address'=>'required',
            'website_id'=>'required',
            'contact_person_name'=>'required'
        ];
    }
}
