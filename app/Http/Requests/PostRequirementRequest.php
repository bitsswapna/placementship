<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequirementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'looking_role_id'=>'required',
            'maximum_no_of_ipcc_attempts'=>'required',
            'qualification_needed'=>'required',
            'minimum_test_mark'=>'required',
            'other_requirements'=>'required',
            'prefered_gender'=>'required'
        ];
    }
}
