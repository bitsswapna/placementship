<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFullDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'qualification_id'=>'required',
            'tenth_passed_year'=>'required',
            'tenth_percentage'=>'required',
            'twelfth_passed_year'=>'required',
            'twelfth_percentage'=>'required',
            'cpt_number_of_attempts'=>'required',
            'ipcc_number_of_attempts'=>'required',
            'degree'=>'required',
            'other_professional_course'=>'required',
            'achievements'=>'required',
            'career_objectives'=>'required',
            'extra_curicular_activities'=>'required',
            'skills'=>'required',
            'other_professional_achievements'=>'required',
            'cpt_percentage' =>'required',
            'cpt_passed_year' =>'required',
            'ipcc_percentage' =>'required',
            'ipcc_passed_year' =>'required',
            'itt_orientation_status'=>'required'
        ];
    }
}
