<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeBasicUpdateRequest;
use App\Http\Requests\EmployeelistRequest;
use App\Http\Requests\EmployeeResumeUpdateRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Http\Requests\EmployeeViewRequest;
use App\Http\Requests\UserApplicationRequest;
use App\Models\FirmRequirement;
use App\Models\User as ModelsUser;
use App\Models\UserApplication;
use App\Models\UserCoursePursuing;
use App\Models\UserDetails;
use App\Models\UserExamAnswer;
use App\Models\UserExamDetail;
use App\Models\UserWorkExperience;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Bridge\UserRepository;

class AdminUserController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function getCounts(){
        try {

            $studentcount = ModelsUser::where(['role_id'=>2])->where('users.deleted_at', NULL)->count();
            $employeecount = ModelsUser::where(['role_id'=>3])->where('users.deleted_at', NULL)->count();
            $firmcount = ModelsUser::where(['role_id'=>4])->where('users.deleted_at', NULL)->count();
            $allrequirementcount=FirmRequirement::where('firm_requirements.deleted_at', NULL)->count();
            $pendingrequirementcount=FirmRequirement::where(['approval_status'=>0])->where('firm_requirements.deleted_at', NULL)->count();
            $expiredrequirementcount=FirmRequirement::where(['approval_status'=>2])->where('firm_requirements.deleted_at', NULL)->count();
            $approvedrequirementcount=FirmRequirement::where(['approval_status'=>1])->where('firm_requirements.deleted_at', NULL)->count();
            return response()->json(['message'=>'Counts','studentcount'=>$studentcount,'employeecount'=>$employeecount,'firmcount'=> $firmcount,'allrequirementcount'=>$allrequirementcount,'pendingrequirementcount'=>$pendingrequirementcount,'expiredrequirementcount'=>$expiredrequirementcount,'approvedrequirementcount'=>$approvedrequirementcount,'status'=>true],200);
        }
        catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function getAllemployees(EmployeelistRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;
            $sort = $request->sort;
            $filter = $request->filter;
            // $filter_by=$request->filter_by;
            // $filter_value=$request->filter_value;



            if(!empty($sort)){
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='users.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='users.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "score" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='user_exam_details.total_score';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "score" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='user_exam_details.total_score';
                    $sort_order1=$sort[0]['sort_order'];
            }else{
                $sort_by1='users.id';
                $sort_order1="DESC";
            }


            if(($request->role_id==2) || ($request->role_id==3))
            {
                $results = ModelsUser::leftjoin('user_exam_details','user_exam_details.user_id','users.id')
                ->leftjoin('user_details','user_details.user_id','users.id')
                ->leftJoin('roles', 'roles.id','users.role_id')
                ->leftjoin('qualifications','qualifications.id','user_details.qualification_id')
                // ->leftjoin('courses','courses.id','user_details.course_id')
                ->with(['getAllCoursesPursuing.getCourseName'=> function ($query) {
                    $query->select('courses.id','courses.name as course_name');

                }])
                ->with('location')
                ->with('preferedLocation')
                ->where(function($query) use ($filter) {
                    if($filter!=NULL){
                        for($i=0;$i<count($filter);$i++){
                            if($filter[$i]['filter_by'] == 'appointed_status'){

                                $query->where('user_details.selection_status', 'like', '%'.$filter[$i]['filter_value'].'%');
                            }
                            if($filter[$i]['filter_by'] == 'location'){

                                $query->orWhere('users.prefered_location_to_work', 'like', '%'.$filter[$i]['filter_value'].'%');
                            }
                            if($filter[$i]['filter_by'] == 'qualification'){

                                $query->orWhere('user_details.qualification_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                            }
                            if($filter[$i]['filter_by']=='course')
                            {

                                $searchterm =$filter[$i]['filter_value'];
                                $relationAttribute = 'user_course_pursuings.course_id';
                                $query->orWhereHas('getAllCoursesPursuing', function (Builder $query) use ($relationAttribute, $searchterm) {
                                    $query->where($relationAttribute, 'like', '%'.$searchterm.'%');
                                });
                            }
                            if($filter[$i]['filter_by'] == 'profile_status'){

                                $query->orWhere('users.profile_status', 'like', '%'.$filter[$i]['filter_value'].'%');
                            }

                        }
                    }
                })

                ->where('users.role_id',$request->role_id)
                ->where('users.deleted_at', NULL)
                ->orderBy($sort_by1,$sort_order1)
                ->select( 'users.id','roles.name as role','users.sro_number','users.name','users.email',
                'users.profile_status','users.location_zone','users.prefered_location_to_work',
                DB::raw("CASE WHEN user_details.selection_status='0' THEN 'Not appointed' ELSE 'appointed' END as selection_status")
                ,'user_details.qualification_id','qualifications.name as qualification','users.mobile',
                DB::raw("CASE WHEN users.profile_status='0' THEN 'Registered' WHEN users.profile_status='1' THEN 'Completed' WHEN users.profile_status='2' THEN 'Attended Exam' WHEN users.profile_status='3' THEN 'Resume Uploaded' WHEN users.profile_status='4' THEN 'Password Set' ELSE 'Mobile Verified' END as profile_status"),
                'user_exam_details.total_score',DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as created_date'))

                ->withCount('getAllApplications')
                ->withCount(['getAllShortlisted'=> function ($query) {
                    $query->where('user_applications.application_status', 1);
                }]);

                $count= $results->count();
                $result=$results->skip($start)->limit($limit)->distinct()->get();
                return    response()->json(['message'=>'Student/Employee List',
                'list' => $result,
                'count'=>$count,
                'status'=>true],200);
            }else{
                return    response()->json(['message'=>'Student/Employee List','list' => '','status'=>false],200);
            }

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function getAuser(EmployeeViewRequest $request){
        try {
            $result = ModelsUser::leftjoin('user_exam_details','user_exam_details.user_id','users.id')
            ->leftjoin('user_details','user_details.user_id','users.id')
            ->leftJoin('roles', 'roles.id','users.role_id')
            ->leftjoin('qualifications','qualifications.id','user_details.qualification_id')
                // ->leftjoin('courses','courses.id','user_details.course_id')

            ->with(['getAllWorkExperiences'])
            ->with(['getAllCoursesPursuing.getCourseName'=> function ($query) {
                //$join->on('courses','courses.id','user_course_pursuings.course_id');
                $query->select('courses.id','courses.name as course_name');

            }])
            ->with('location')
            ->with('preferedLocation')
            // ->with(['getAllShortlisted'=> function ($query) {
            //     $query->where('user_applications.application_status', 1);
            // }])
            ->where('users.id',$request->id)
            ->select('users.id','roles.name as role','users.name',DB::raw('DATE_FORMAT(users.dob, "%d-%b-%Y") as dob'),'users.email','users.sro_number','users.location_zone','users.prefered_location_to_work',
            DB::raw("CASE WHEN users.gender='0' THEN 'Male' WHEN users.gender='1' THEN 'Female' ELSE 'Others' END as gender"),'user_exam_details.total_score',
            DB::raw("CASE WHEN user_details.selection_status='0' THEN 'Not appointed' ELSE 'appointed' END as appointed_status"),
            'users.address','user_details.college_name','user_details.tenth_passed_year','user_details.tenth_percentage','user_details.twelfth_passed_year',
            'user_details.twelfth_percentage','user_details.cpt_number_of_attempts','user_details.ipcc_number_of_attempts','user_details.degree','user_details.other_professional_course',
            'user_details.achievements','user_details.career_objectives','user_details.extra_curicular_activities','user_details.skills',
            'user_details.other_professional_achievements','user_details.resume',
            'user_details.cpt_percentage','user_details.cpt_passed_year',
            'user_details.ipcc_percentage','user_details.ipcc_passed_year',
            'user_details.languages_known','user_details.employment_type_id','user_details.ipce_coaching_institute','user_details.type_of_exposure',
            DB::raw("CASE WHEN user_details.itt_orientation_status='0' THEN 'incompleted' ELSE 'completed' END as itt_orientation_status")
            ,'user_details.qualification_id','qualifications.name as qualification'
            ,'users.mobile',DB::raw("CASE WHEN users.profile_status='0' THEN 'Registered' WHEN users.profile_status='1' THEN 'Completed' WHEN users.profile_status='2' THEN 'Attended Exam' WHEN users.profile_status='3' THEN 'Resume Uploaded' WHEN users.profile_status='4' THEN 'Password Set' ELSE 'Mobile Verified' END as profile_status")
            ,'user_exam_details.total_score',
            DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as joined_date'))
            ->withCount('getAllApplications')
            ->withCount(['getAllShortlisted'=> function ($query) {
                $query->where('user_applications.application_status', 1);
            }])
            ->get();
            return    response()->json(['message'=>'Details of a Student/Employee','details' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function editBasicsUser(EmployeeBasicUpdateRequest $request){
        try {

            $userDetails = ModelsUser::find($request->input('id'));
            $userDetails->name = $request->input('name');
            $userDetails->email = $request->input('email');
            $userDetails->sro_number = $request->input('sro_number');
            $userDetails->dob = $request->input('dob');
            $userDetails->location_zone = $request->input('location_zone');
            $userDetails->prefered_location_to_work = $request->input('prefered_location_to_work');
            $userDetails->address = $request->input('address');
            $userDetails->gender = $request->input('gender');
            $userDetails->mobile = $request->input('mobile');

            $userDetails->save();


            $userDetailss =UserDetails::where('user_id',$request->input('id'))->first();

            $userDetailss->qualification_id = $request->input('qualification_id');

            $userDetailss->tenth_passed_year = $request->input('tenth_passed_year');
            $userDetailss->tenth_percentage = $request->input('tenth_percentage');
            $userDetailss->twelfth_passed_year = $request->input('twelfth_passed_year');
            $userDetailss->twelfth_percentage = $request->input('twelfth_percentage');
            $userDetailss->cpt_number_of_attempts = $request->input('cpt_number_of_attempts');
            $userDetailss->ipcc_number_of_attempts = $request->input('ipcc_number_of_attempts');

            $userDetailss->cpt_percentage = $request->input('cpt_percentage');
            $userDetailss->cpt_passed_year = $request->input('cpt_passed_year');
            $userDetailss->ipcc_percentage = $request->input('ipcc_percentage');
            $userDetailss->ipcc_passed_year = $request->input('ipcc_passed_year');
            $userDetailss->itt_orientation_status = $request->input('itt_orientation_status');

            $userDetailss->degree = $request->input('degree');
            $userDetailss->other_professional_course = $request->input('other_professional_course');
            $userDetailss->achievements = $request->input('achievements');
            // $userDetailss->awards = $request->input('awards');
            $userDetailss->career_objectives = $request->input('career_objectives');

            $userDetailss->extra_curicular_activities = $request->input('extra_curicular_activities');
            $userDetailss->skills = $request->input('skills');
            $userDetailss->other_professional_achievements = $request->input('other_professional_achievements');
            // $userDetailss->selection_status = $request->input('selection_status');
            $userDetailss->languages_known = $request->input('languages_known');
            $userDetailss->employment_type_id = $request->input('employment_type_id');
            $userDetailss->ipce_coaching_institute = $request->input('ipce_coaching_institute');
            $userDetailss->type_of_exposure = $request->input('type_of_exposure');

            $userDetailss->save();

            if($request->input('work_experiences')){
                $works=$request->input('work_experiences');
                UserWorkExperience::where('user_id',$userDetails->id)->delete();
                foreach ($works as $work) {
                        $res=new UserWorkExperience();
                        $res->user_id= $userDetails->id;
                        $res->firm_name= $work['firm_name'];
                        $res->no_of_years= $work['no_of_years'];
                        $res->area= $work['area'];
                        $res->save();
                }
            }
            if($request->input('courses')){
                $courses=$request->input('courses');
                UserCoursePursuing::where('user_id',$userDetails->id)->delete();
                foreach ($courses as $course) {

                        $res=new UserCoursePursuing();
                        $res->user_id= $userDetails->id;
                        $res->course_id= $course['course_id'];
                        $res->save();

                }

            }


            return    response()->json(['message'=>'Successfully Updated','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }

    public function updateResume(EmployeeResumeUpdateRequest $request){
        try {
            if ($files = $request->file('resume')) {
                //store file into document folder

                //$file = uploadAnyFile('documents', $request->file('resume'));
                $filename = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $picture = date('His').$filename;
                $path ='/documents';
                $destinationPath = public_path().'/documents';

                $file = $request->file('resume') ? $files->move($destinationPath, $picture) : null;

                $document = UserDetails::where('user_id',$request->id)->update([
                    'resume' =>$path.'/'.$picture]);


                // $document->resume = $file;
                // $document->update();
                return    response()->json(['message'=>'Successfully Updated','status'=>true],200);
            }
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function deleteUser(Request $request){
        try {
            $d=ModelsUser::where(['id'=> $request->id])->delete();
            UserDetails::where(['user_id'=> $request->id])->delete();
            UserApplication::where(['user_id'=> $request->id])->delete();
            UserCoursePursuing::where(['user_id'=> $request->id])->delete();
            UserWorkExperience::where(['user_id'=> $request->id])->delete();
            UserExamAnswer::where(['user_id'=> $request->id])->delete();
            UserExamDetail::where(['user_id'=> $request->id])->delete();
            return    response()->json(['message'=>'Successfully Deleted','status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewUserApplication(UserApplicationRequest $request){
        try {
            $limit=$request->limit;
            $start=$request->start;
            $result = UserApplication::leftjoin('users','user_applications.user_id','users.id')
            ->leftjoin('user_exam_details','user_exam_details.user_id','user_applications.user_id')
            ->leftjoin('firm_requirements',function ($join){
                $join->on("firm_requirements.id","user_applications.firm_requirement_id");
                //$join->with("getFirm");
            })

          ->where('user_applications.user_id',$request->id)
            ->select('user_applications.id','users.name as user_name','user_exam_details.total_score',
            'firm_requirements.id as firm_id',
            DB::raw("CASE WHEN user_applications.application_status='0' THEN 'applied' WHEN user_applications.application_status='1' THEN 'short listed' ELSE 'Offer letter recieved' END as application_status"),'user_exam_details.total_score',
            DB::raw('DATE_FORMAT(user_applications.created_at, "%d-%b-%Y") as applied_date'))
            //->with('getFirmRequirement')
            ->skip($start)->limit($limit)->get();
            return    response()->json(['message'=>'Student/Employee Applications List','list'=>$result,'status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewUserShortListedApplication(UserApplicationRequest $request){
        try {
            $limit=$request->limit;
            $start=$request->start;
            $result = UserApplication::leftjoin('users','user_applications.user_id','users.id')
            ->leftjoin('user_exam_details','user_exam_details.user_id','user_applications.user_id')
            ->leftjoin('firm_requirements',function ($join){
                $join->on("firm_requirements.id","user_applications.firm_requirement_id");
                //$join->with("getFirm");
            })
          ->where(['user_applications.user_id'=>$request->id,'user_applications.application_status'=>1])
            ->select('user_applications.id','users.name as user_name','user_exam_details.total_score',
            'firm_requirements.id as firm_id',
            DB::raw("CASE WHEN user_applications.application_status='0' THEN 'applied' WHEN user_applications.application_status='1' THEN 'short listed' ELSE 'Offer letter recieved' END as application_status"),'user_exam_details.total_score',
            DB::raw('DATE_FORMAT(user_applications.created_at, "%d-%b-%Y") as applied_date'))
            //->with('getFirmRequirement')
            ->skip($start)->limit($limit)->get();
            return    response()->json(['message'=>'Student/Employee Shortlisted Applications List','list'=>$result,'status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
}
