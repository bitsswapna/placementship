<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationAddRequest;
use App\Http\Requests\LocationeditRequest;
use App\Http\Requests\LocationlistRequest;
use App\Http\Requests\LocationViewRequest;
use App\Models\LocationZone;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    //
    public function getAllLocations(LocationlistRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;
            $result = LocationZone::select('id','location');

            $count= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            return    response()->json(['message'=>'Locations/Zone List','list' => $results,'count'=>$count,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function addAllLocations(LocationAddRequest $request){
        try {

            $result = LocationZone::create([
                'location' =>$request->input('location')
            ]);

            return    response()->json(['message'=>'Location added Successfully','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewLocations(LocationViewRequest $request){
        try {

            $result = LocationZone::where('id',$request->id)->get();

            return    response()->json(['message'=>'Locations/Zone ','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function editLocations(LocationeditRequest $request){
        try {

            $result = LocationZone::where('id',$request->id)->update([
                'location' =>$request->input('location')
            ]);

            return    response()->json(['message'=>'Location Updated Successfully','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function deleteLocations(Request $request){
        try {
            $d=LocationZone::where(['id'=> $request->id])->delete();
            return    response()->json(['message'=>'Successfully Deleted','status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
}
