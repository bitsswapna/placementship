<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseRequest;
use App\Http\Requests\QualificationRequest;
use App\Models\Course;
use App\Models\LocationZone;
use App\Models\NumberOfPartner;
use App\Models\Qualification;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    //
    public function getAllQualifications(QualificationRequest $request){
        try {

            $result = Qualification::select('id','name')->get();
            return    response()->json(['message'=>'Qualification List','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function getAllCourses(CourseRequest $request){
        try {

            $result = Course::select('id','name')->get();
            return    response()->json(['message'=>'Courses List','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function getAllLocations(Request $request){
        try {

            $result = LocationZone::select('id','location')->get();
            return    response()->json(['message'=>'Loacation List','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function getAllNumberOfPartners(Request $request){
        try {

            $result = NumberOfPartner::select('id','no_of_partners')->get();
            return    response()->json(['message'=>'Number Of Partners List','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
}
