<?php

namespace App\Http\Controllers;

use App\Http\Requests\AttendExamRequest;
use App\Http\Requests\ExamListRequest;
use App\Models\ExamQuestion;
use App\Models\ExamTime;
use App\Models\UserExamAnswer;
use App\Models\UserExamDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserExamController extends Controller
{
    //
    public function getAllQuestions(ExamListRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;
            $result = ExamQuestion::where('exam_questions.deleted_at', NULL)
            // ->with('getAnswers')
            // ->with(['getCorrectOption'])
            ->select('id','question','mark','option1','option2','option3','option4',
            DB::raw("CASE WHEN correct_option='1' THEN 'A' WHEN correct_option='2' THEN 'B' WHEN correct_option='3' THEN 'C' ELSE 'D' END as correct_option")
            ,DB::raw('DATE_FORMAT(created_at, "%d-%b-%Y") as created_date'));

            $count =  $result->sum('mark');
            $time =  ExamTime::select('time')->get();

            $counts= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            return    response()->json(['message'=>'List of all questions','total_time'=>$time,'total_mark'=>$count,'list' => $results,'question_count'=>$counts,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function attendExam(AttendExamRequest $request){
        try {

            if($request->input('user_exam_answers')){
                // UserExamAnswer::where('user_id',Auth::User()->id)->delete();
                // UserExamDetail::where('user_id',Auth::User()->id)->delete();
                $useranswers=$request->input('user_exam_answers');
                foreach ($useranswers as $useranswer) {
                    $checkStatus = ExamQuestion::where('id',$useranswer['question_id'])->first();

                    if($checkStatus){
                        if($checkStatus['correct_option']== $useranswer['answer_option']){
                            //$status=1;
                            $score=$checkStatus['mark'];
                        }else{
                            //$status=0;
                            $score=0;
                        }
                    }else{
                        //$status=0;
                        $score=0;
                    }

                        $result=new UserExamAnswer();
                        $result->user_id = Auth::User()->id;
                        $result->question_id = $useranswer['question_id'];
                        $result->answer_option = $useranswer['answer_option'];

                        //$result->answer_status = $status;
                        $result->score = $score;
                        $result->save();
                }
            }


            $marks = UserExamAnswer::where('user_id',Auth::User()->id)->select('id','user_id','score')->get();
            $t_score =  $marks->sum('score');

            $results = new UserExamDetail();
            $results->user_id = Auth::User()->id;
            $results->exam_attended_status = 1;
            $results->total_score = $t_score;
            $results->save();

            return    response()->json(['message'=>'Success',
            'status'=>true],200);


        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
}
