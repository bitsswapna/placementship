<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExamListRequest;
use App\Http\Requests\ExamTimeUpdateRequest;
use App\Http\Requests\QuestionAddRequest;
use App\Http\Requests\QuestionDeleteRequest;
use App\Http\Requests\QuestionEditRequest;
use App\Http\Requests\QuestionViewRequest;
use App\Models\ExamAnswer;
use App\Models\ExamQuestion;
use App\Models\ExamTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    //
    public function getAllQuestions(ExamListRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;
            $result = ExamQuestion::where('exam_questions.deleted_at', NULL)
            // ->with('getAnswers')
            // ->with(['getCorrectOption'])
            ->select('id','question','mark','option1','option2','option3','option4',
            DB::raw("CASE WHEN correct_option='1' THEN 'A' WHEN correct_option='2' THEN 'B' WHEN correct_option='3' THEN 'C' ELSE 'D' END as correct_option")
            ,DB::raw('DATE_FORMAT(created_at, "%d-%b-%Y") as created_date'));

            $count =  $result->sum('mark');
            $time =  ExamTime::select('time')->get();

            $counts= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            return    response()->json(['message'=>'List of all questions','total_time'=>$time,'total_mark'=>$count,'list' => $results,'count'=>$counts,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function addQuestions(QuestionAddRequest $request){
        try {

            $questionDetails = new ExamQuestion();
            $questionDetails->question =$request->input('question');
            //$questionDetails->right_option_id = $request->input('right_option_id');
            $questionDetails->mark = $request->input('mark');
            $questionDetails->option1 = $request->input('option1');
            $questionDetails->option2 = $request->input('option2');
            $questionDetails->option3 = $request->input('option3');
            $questionDetails->option4 = $request->input('option4');
            $questionDetails->correct_option = $request->input('correct_option');
            $data=$questionDetails->save();

            // for ($i = 1; $i <= 4; $i++) {
            //     $answers[] = [
            //         'question_id' => $questionDetails->id,
            //         'answer_order' =>$request->answer_order[$i],
            //         'answer_option' => $request->answer_option[$i],
            //         'correct_option' => $request->correct_option[$i]
            //     ];
            // }
            // ExamAnswer::insert($answers);

            // $result = ExamAnswer::create([
            //     'question_id'=>$questionDetails->id,
            //     'answer_order' =>$request->input('answer_order'),
            //     'answer_option' => $request->input('answer_option'),
            //     'correct_option' => $request->input('correct_option')

            // ]);

            return    response()->json(['message'=>'Successfully Added','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }

    public function editQuestions(QuestionEditRequest $request){
        try {

            $questionDetails = ExamQuestion::where('id',$request->id)->update([
                'question' =>$request->input('question'),
                'mark' => $request->input('mark'),
                'option1' => $request->input('option1'),
                'option2' => $request->input('option2'),
                'option3' => $request->input('option3'),
                'option4' => $request->input('option4'),
                'correct_option' => $request->input('correct_option')
            ]);

            return    response()->json(['message'=>'Successfully Updated','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }

    public function addExamTime(ExamTimeUpdateRequest $request){
        try {
            $questionDetails =  ExamTime::first();
            if($questionDetails==NULL){
                $data = new ExamTime();
                $data->time=$request->input('time');
                $data->save();
                return    response()->json(['message'=>'Successfully Added','status'=>true],200);

            }else{
                ExamTime::first()->update([
                'time' =>$request->input('time')
                ]);
                return    response()->json(['message'=>'Successfully Updated','status'=>true],200);
            }

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewQuestions(QuestionViewRequest $request){
        try {

            $result = ExamQuestion::where('id',$request->id)->get();

            return    response()->json(['message'=>'Question ','view' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function deleteQuestions(QuestionDeleteRequest $request){
        try {
            $d=ExamQuestion::where(['id'=> $request->id])->delete();
            return    response()->json(['message'=>'Successfully Deleted','status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
}
