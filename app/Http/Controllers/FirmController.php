<?php

namespace App\Http\Controllers;

use App\Http\Requests\FirmAddRequest;
use App\Http\Requests\FirmBlockRequest;
use App\Http\Requests\FirmDeleteRequest;
use App\Http\Requests\FirmGalleryAddRequest;
use App\Http\Requests\FirmGalleryRequest;
use App\Http\Requests\FirmListRequest;
use App\Http\Requests\FirmRequirementRequest;
use App\Http\Requests\FirmUpdateRequest;
use App\Http\Requests\FirmViewRequest;
use App\Models\FirmDetail;
use App\Models\FirmGallery;
use App\Models\FirmRequirement;
use Illuminate\Http\Request;
use App\Models\User as ModelsUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class FirmController extends Controller
{
    //
    public function getAllFirms(FirmListRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;

            $sort = $request->sort;
            $filter = $request->filter;



            if(!empty($sort)){
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='users.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='users.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "no_of_requirements" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='get_all_requirements_count';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "no_of_requirements" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='get_all_requirements_count';
                    $sort_order1=$sort[0]['sort_order'];
            }else{
                $sort_by1='users.id';
                $sort_order1="DESC";
            }
            $sort_by1='users.id';

            $result = ModelsUser::leftjoin('firm_details','firm_details.user_id','users.id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('number_of_partners','number_of_partners.id','firm_details.no_of_partners_id')
            ->where(function($query) use ($filter) {
                if($filter!=NULL){
                    for($i=0;$i<count($filter);$i++){
                        if($filter[$i]['filter_by'] == 'location_zone'){

                            $query->where('users.location_zone', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }
                        if($filter[$i]['filter_by'] == 'no_of_partners'){

                            $query->orWhere('firm_details.no_of_partners_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }
                        if($filter[$i]['filter_by'] == 'block_status'){

                            $query->orWhere('firm_details.block_status', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }

                    }
                }
            })
            ->where('users.role_id',4)
            ->where('users.deleted_at', NULL)
            ->select('users.id','users.name as firm_name','users.mobile','users.email','users.location_zone','location_zones.location',
            DB::raw("CASE WHEN firm_details.block_status='0' THEN 'Blocked' ELSE 'Active' END as block_status"),
            'firm_details.no_of_partners_id',DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as created_date'),'number_of_partners.no_of_partners')
            ->withCount('getAllRequirements')
            ->orderBy(DB::raw($sort_by1),$sort_order1);

            $count= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            return    response()->json(['message'=>'Firm List','list' => $results,'count'=>$count,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function addFirms(FirmAddRequest $request){
        try {


            $userDetails = new ModelsUser();
            $userDetails->role_id =4;
            $userDetails->name = $request->input('name');
            $userDetails->email = $request->input('email');
            $userDetails->location_zone = $request->input('location_zone');
           // $userDetails->address = $request->input('address');
            $userDetails->mobile = $request->input('mobile');
            $userDetails->address = $request->input('address');
            $userDetails->website_id = $request->input('website_id');

            $userDetails->save();


            $result = new FirmDetail();
            $result->user_id=$userDetails->id;
            $result->no_of_partners_id =$request->input('no_of_partners_id');
            $result->exposure_offered = $request->input('exposure_offered');
            $result->about_firm = $request->input('about_firm');
            $result->stipened_offered = $request->input('stipened_offered');
            $result->contact_person_name = $request->input('contact_person_name');
            $result->save();

            return    response()->json(['message'=>'Successfully Added','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function editFirms(FirmUpdateRequest $request){
        try {


            $userDetails = ModelsUser::find($request->input('id'));
            $userDetails->name = $request->input('name');
            $userDetails->email = $request->input('email');
            $userDetails->location_zone = $request->input('location_zone');
            $userDetails->address = $request->input('address');
            $userDetails->mobile = $request->input('mobile');
            $userDetails->website_id = $request->input('website_id');

            $data=$userDetails->save();



            $result = FirmDetail::where('user_id',$userDetails->id)->update([
                'no_of_partners_id' =>$request->input('no_of_partners_id'),
                'exposure_offered' => $request->input('exposure_offered'),
                'about_firm' => $request->input('about_firm'),
                'stipened_offered' => $request->input('stipened_offered'),
                'contact_person_name' =>$request->input('contact_person_name')

            ]);

            return    response()->json(['message'=>'Successfully Updated','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewFirm(FirmViewRequest $request){
        try {


            $result = ModelsUser::leftjoin('firm_details','firm_details.user_id','users.id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('number_of_partners','number_of_partners.id','firm_details.no_of_partners_id')
            ->where('users.id',$request->id)
            ->with('getFirmGallery')
            ->select('users.id','users.name as firm_name','users.mobile','users.email','users.address','users.location_zone',
            'location_zones.location','users.website_id',
            DB::raw("CASE WHEN firm_details.block_status='0' THEN 'Blocked' ELSE 'Active' END as block_status"),
            'number_of_partners.no_of_partners','firm_details.no_of_partners_id',
            'firm_details.contact_person_name','firm_details.exposure_offered',
            'firm_details.stipened_offered','firm_details.about_firm',
            DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as created_date'))
            ->withCount('getAllRequirements')
            ->first();
            return    response()->json(['message'=>'Details of a Firm','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function deleteFirm(FirmDeleteRequest $request){
        try {
            $d=ModelsUser::where(['id'=> $request->id])->delete();
            FirmDetail::where(['user_id'=> $request->id])->delete();
            FirmRequirement::where(['user_id'=> $request->id])->delete();
            return    response()->json(['message'=>'Successfully Deleted','status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function blockFirm(FirmBlockRequest $request){
        try {

            FirmDetail::where(['user_id'=> $request->id])->update([
                'block_status' =>$request->block_status
            ]);

            return    response()->json(['message'=>'Successfully blocked','status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewFirmRequirements(FirmRequirementRequest $request){
        try {
            $result = ModelsUser::leftjoin('firm_details','firm_details.user_id','users.id')

            ->where('users.id',$request->id)

            ->select('users.id','users.name as firm_name')
            ->with(['getAllRequirements'=> function ($query) {

                $query->where('firm_requirements.deleted_at', NULL);
                $query->select('firm_requirements.id as requirement_id','firm_requirements.user_id',
                DB::raw("CASE WHEN firm_requirements.looking_role_id='2' THEN 'Student' ELSE 'Employee' END as looking_role")
                ,DB::raw("CASE WHEN firm_requirements.entry_type='0' THEN 'Direct Entry' ELSE 'IPCC group requirement' END as entry_type"),
                'firm_requirements.maximum_no_of_ipcc_attempts','firm_requirements.qualification_needed',
                'firm_requirements.work_experience','firm_requirements.area',
                'firm_requirements.minimum_test_mark','firm_requirements.prefered_gender',
                'firm_requirements.other_requirements'
                ,DB::raw("CASE WHEN firm_requirements.approval_status='0' THEN 'Pending' WHEN firm_requirements.approval_status='1' THEN 'Approved' ELSE 'Expired' END as approval_status")
                ,DB::raw('DATE_FORMAT(firm_requirements.created_at, "%d-%b-%Y") as created_date'));
            }])

            ->get();
            return    response()->json(['message'=>'Get Firm Requirements','list' => $result,'status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function addFirmGallery(FirmGalleryRequest $request){
        //dd('hi');
        try {

            if ($request->hasFile('image')) {

                $files = $request->file('image');
               // $res=count($files);

                //if (count($files) > 0) {
                    foreach($files as $file){

                        $filename = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $picture = date('His').$filename;
                        $destinationPath = public_path().'/uploads';
                        $path ='/uploads';
                        $file->move($destinationPath, $picture);

                        $res = new FirmGallery();
                        $res->user_id=$request->input('id');
                        $res->image_path=$path.'/'.$picture;
                        $res->save();

                    }
               // }
            }

            return    response()->json(['message'=>'Successfully Uploaded','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }

}
