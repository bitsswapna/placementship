<?php

namespace App\Http\Controllers;

use App\Http\Requests\FirmEditRequest;
use App\Http\Requests\FirmGalleryRequest;
use App\Http\Requests\FirmLoginRequest;
use App\Http\Requests\FirmRegisterRequest;
use App\Http\Requests\FirmViewRequest;
use App\Http\Requests\MobileOtpResendRequest;
use App\Http\Requests\MobileOtpVerificationRequest;
use App\Http\Requests\PasswordsetRequest;
use App\Http\Requests\RegisterFirmFullDetailsRequest;
use App\Models\FirmDetail;
use App\Models\FirmGallery;
use App\Models\User;
use App\Models\UserDetails;
use App\Models\UserOtp;
use Carbon\Carbon;
use File;
use Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Input;

class FirmAuthController extends Controller
{
    //
    public function login(FirmLoginRequest $request)
    {
        // try {

            $rules = [
                'mobile' => 'required|mobile',
                'password' => 'required|string',
                'remember_me' => 'boolean'
            ];

            $customMessages = [
                'required' => 'The :attribute field is required.'
            ];
           // $this->validate($request, $rules, $customMessages);

           if($request->mobile!=NULL){
            $credentials = request(['mobile', 'password']);
            }


            if(!Auth::attempt($credentials))
            {
                return response()->json([
                    'access_token' =>'',
                    'token_type' => '',
                    'expires_at' =>'',
                    'message' => 'Invalid login credentials',
                    'data'=>'',
                    'status'=>false
                ], 200);
            }else{

                $user = $request->user();
                //dd($user->role_id);
                if ($user->accessTokens->count() > 0) {
                    $user->accessTokens()->delete();
                }
                if(($user->role_id==1) || ($user->role_id==2) || ($user->role_id==3)){
                    return response()->json([
                        'access_token' =>'',
                        'token_type' => '',
                        'expires_at' =>'',
                        'message' => 'Invalid User',
                        'data'=>'',
                        'status'=>false
                    ], 200);
                }else{
                    $tokenResult = $user->createToken('Personal Access Token');
                    $token = $tokenResult->token;
                    if ($request->remember_me)
                        $token->expires_at = Carbon::now()->addWeeks(1);
                    $token->save();
                    return response()->json([
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse(
                            $tokenResult->token->expires_at
                        )->toDateTimeString(),
                        'message'=>'Successfully Logged In',
                        'data'=>$user,
                        'status'=>true
                    ]);
                }
            }
        // }catch (\Exception $exception){
        //     return response()->json(['message'=>$exception->getMessage()],500);
        // }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->delete();
        return response()->json([
            'message' => 'Successfully logged out',
            'status'=>true
        ], 200);
    }
    public function signup(FirmRegisterRequest $request){

        try {
            if($request->input('name')){
                $userDetails = new User();
                $userDetails->name = $request->input('name');
                $userDetails->email = $request->input('email');
                $userDetails->role_id = 4;
                $userDetails->address = $request->input('address');
                $userDetails->mobile = $request->input('mobile');
                $data=$userDetails->save();

                $res = new FirmDetail();
                $res->user_id=$userDetails->id;
                $res->contact_person_name=$request->input('contact_person_name');
                $res->save();

                $otp = $this->generateOTP();
                $storeotp = new UserOtp();
                $storeotp->user_id= $userDetails->id;
                $storeotp->otp=$otp;
                $storeotp->save();
                $message = 'you otp is '.$otp;



                return response()->json([
                    'message' => 'Successfully Registered and an OTP is sent to your mobile number',
                    'status'=>true,
                    'otp'=>$otp,
                    'user_id'=>$userDetails->id
                ], 200);
            }


        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function generateOTP(){
        $otp = mt_rand(1000,9999);
        return $otp;
    }
    public function mobileVerification(MobileOtpVerificationRequest $request){
        try {
            if($request->input('otp')){
                $getOtp = UserOtp::where('user_id',$request->id)->get();

                if($request->input('otp') == $getOtp[0]->otp){
                    UserOtp::where(['user_id'=> $request->id])->delete();
                    return response()->json([
                        'message' => 'Success',
                        'user_id'=>$request->id,
                        'status'=>true
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'Otp Not Valid',
                        'status'=>false
                    ], 200);
                }

            }

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function mobileOtpResend(MobileOtpResendRequest $request){
        try {
                UserOtp::where(['user_id'=> $request->id])->delete();
                $otp = $this->generateOTP();
                $storeotp = new UserOtp();
                $storeotp->user_id= $request->id;
                $storeotp->otp=$otp;
                $storeotp->save();
                $message = 'you otp is '.$otp;
                return response()->json([
                    'message' => 'Successfully Sent',
                    'status'=>true,
                    'otp'=>$otp,
                    'user_id'=>$request->id
                ], 200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function setupPassword(PasswordsetRequest $request){
        try {
            User::where('id', $request->input('id'))->update(['password' => bcrypt($request->input('password'))]);
            return response()->json([
                'message' => 'Successfully Updated',
                'status'=>true,
                'user_id'=>$request->id
            ], 200);

            }catch (\Exception $exception){
                return response()->json(['message'=>$exception->getMessage()],500);
            }

    }
    public function signupFullDetails(RegisterFirmFullDetailsRequest $request){
        try{

                $registeru=User::where(['id'=> $request->id])->first();
                $registeru->location_zone=$request->input('location_zone');
                $registeru->website_id=$request->input('website_id');
                $registeru->save();
                $register=FirmDetail::where(['user_id'=> $request->id])->first();
                $register->no_of_partners_id= $request->input('no_of_partners_id');
                $register->exposure_offered=$request->input('exposure_offered');
                $register->stipened_offered= $request->input('stipened_offered');
                $register->about_firm=$request->input('about_firm');
                $register->save();
                return response()->json([
                'message' => 'Successfully Registered',
                'status'=>true,
                'user_id'=>$request->id
            ], 200);

            }catch (\Exception $exception){
                return response()->json(['message'=>$exception->getMessage()],500);
        }

    }
    public function  firmgallery(FirmGalleryRequest $request){
        try{

            if ($request->hasFile('image')) {
                $files = $request->file('image');
               // $res=count($files);

                //if (count($files) > 0) {
                    foreach($files as $file){

                        $filename = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $picture = date('His').$filename;
                        $destinationPath = public_path().'/uploads';
                        $file->move($destinationPath, $picture);

                    }
               // }
            }

                $user = User::where('id',$request->id)->first();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

            return response()->json([
                'message' => 'Successfully Completed your Registration Process',
                'status'=>true,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'data'=>$user
            ], 200);

         }catch (\Exception $exception){
                return response()->json(['message'=>$exception->getMessage()],500);
        }

    }
    public function viewFirm(Request $request){
        try {


            $result = User::leftjoin('firm_details','firm_details.user_id','users.id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('number_of_partners','number_of_partners.id','firm_details.no_of_partners_id')
            ->where('users.id',Auth::user()->id)
            ->select('users.id','users.name as firm_name','users.mobile','users.email','users.address','users.location_zone',
            'location_zones.location','users.website_id',
            DB::raw("CASE WHEN firm_details.block_status='0' THEN 'Blocked' ELSE 'Active' END as block_status"),
            'number_of_partners.no_of_partners','firm_details.no_of_partners_id',
            'firm_details.contact_person_name','firm_details.exposure_offered',
            'firm_details.stipened_offered','firm_details.about_firm',
            DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as created_date'))
            ->withCount('getAllRequirements')
            ->first();
            return    response()->json(['message'=>'Details of a Firm','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function editFirms(FirmEditRequest $request){
        try {


            $userDetails = User::find(Auth::user()->id);
            $userDetails->name = $request->input('name');
            $userDetails->email = $request->input('email');
            $userDetails->location_zone = $request->input('location_zone');
            $userDetails->address = $request->input('address');
            $userDetails->mobile = $request->input('mobile');
            $userDetails->website_id = $request->input('website_id');

            $data=$userDetails->save();



            $result = FirmDetail::where('user_id',$userDetails->id)->update([
                'no_of_partners_id' =>$request->input('no_of_partners_id'),
                'exposure_offered' => $request->input('exposure_offered'),
                'about_firm' => $request->input('about_firm'),
                'stipened_offered' => $request->input('stipened_offered'),
                'contact_person_name' =>$request->input('contact_person_name')

            ]);

            return    response()->json(['message'=>'Successfully Updated','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }


}
