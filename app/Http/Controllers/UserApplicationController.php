<?php

namespace App\Http\Controllers;

use App\Http\Requests\FirmViewRequest;
use App\Http\Requests\UserRequirementList;
use App\Models\FirmRequirement;
use App\Models\User as ModelsUser;
use App\Models\UserApplication;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserApplicationController extends Controller
{
    //
    public function viewApplications(Request $request){
        try {

            $limit=$request->limit;
            $start=$request->start;


            $id = $request->id;
            $firm_id = $request->firm_id;
            $sort=$request->sort;
            $filter=$request->filter;
            //$filter_value=$request->filter_value;'

            $firm = User::where('id',$firm_id)->select('id as firm_id','name as firm_name')->get();

            if(!empty($sort)){
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='user_applications.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='user_applications.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "score" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='user_exam_details.total_score';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "score" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='user_exam_details.total_score';
                    $sort_order1=$sort[0]['sort_order'];
            }else{
                $sort_by1='user_applications.id';
                $sort_order1="DESC";
            }


            $result = UserApplication::leftjoin('users','users.id','user_applications.user_id')
            ->leftjoin('user_details','user_details.user_id','user_applications.user_id')
            ->leftjoin('user_exam_details','user_exam_details.user_id','user_applications.user_id')
            ->leftjoin('qualifications','qualifications.id','user_details.qualification_id')
            // ->where(function($query) use ($filter) {
            //     for($i=0;$i<count($filter);$i++){
            //         if($filter[$i]['filter_by'] == 'ipcc_number_of_attempts'){
            //             $query->where('user_details.ipcc_number_of_attempts', 'like', '%'.$filter[$i]['filter_value'].'%');
            //          }
            //          elseif($filter[$i]['filter_by'] == 'application_status'){
            //             $query->orWhere('user_applications.application_status', 'like', '%'.$filter[$i]['filter_value'].'%');
            //          }
            //          elseif($filter[$i]['filter_by'] == 'gender'){
            //             $query->orWhere('users.gender', 'like', '%'.$filter[$i]['filter_value'].'%');
            //         }

            //         elseif($filter[$i]['filter_by'] == 'qualification'){
            //             $query->orWhere('user_details.qualification_id', 'like', '%'.$filter[$i]['filter_value'].'%');
            //         }

            //     }
            // })
            // ->with(['getWorkExperience'=> function ($query) {
            //     $query->select('id','no_of_years');

            // }])
            ->where('user_applications.user_id',$id)
            ->select('user_applications.id','users.id as user_id','users.name as user_applied','user_exam_details.total_score as user_mark',
            DB::raw("CASE WHEN users.role_id='2' THEN 'Student' ELSE 'Employee' END as type")
            ,DB::raw("CASE WHEN users.gender='0' THEN 'Male' ELSE 'Female' END as user_gender"),
            'user_details.ipcc_number_of_attempts',
            'qualifications.name as qualification','users.gender',
            'user_applications.application_status as application_status'
            ,DB::raw('DATE_FORMAT(user_applications.created_at, "%d-%b-%Y") as posted_date'))
            ->orderBy($sort_by1,$sort_order1);


            $count= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            return    response()->json(['message'=>'Firm Application List','list' => $results,'firm_name'=>$firm,'count'=>$count,'status'=>true],200);
        }
        catch (\Exception $exception){
        }


    }
    public function applyRequirement(Request $request){

        try {
            if($request->input('firm_requirement_id')){
                $result = new UserApplication();
                $result->user_id = Auth::User()->id;
                $result->firm_requirement_id = $request->input('firm_requirement_id');
                $result->application_status = 'applied';
                $result->save();
                return    response()->json(['message'=>'Successfully Applied','status'=>true],200);
            }else{
                return    response()->json(['message'=>'Failure','status'=>false],200);
            }

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function getAllRequirements(UserRequirementList $request){
        try {

            $limit=$request->limit;
            $start=$request->start;



            $sort_by=$request->sort_by;
            $filter=$request->filter;

            $user_id =Auth::user()->id;

            if($sort_by!=NULL){
                if($sort_by == "application_status")
                    $sort_by1='user_applications.application_status';
                if($sort_by == "firm_zone")
                    $sort_by1='users.location_zone';

            }else{
                $sort_by1='firm_requirements.id';
            }

            $uid=Auth::user()->id;

            $result = FirmRequirement::leftjoin('users','users.id','firm_requirements.user_id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('qualifications','qualifications.id','firm_requirements.qualification_needed')

            ->leftjoin('firm_details','firm_details.user_id','users.id')

            ->leftjoin('number_of_partners','number_of_partners.id','firm_details.no_of_partners_id')
            // ->leftjoin('user_applications','user_applications.firm_requirement_id','firm_requirements.id')

            ->where(function($query) use ($filter) {
                if($filter!=NULL){
                for($i=0;$i<count($filter);$i++){

                    if($filter[$i]['filter_by'] == 'date_posted'){
                        $query->where('firm_requirements.created_at', 'like', '%'.$filter[$i]['filter_value'].'%');
                     }
                     elseif($filter[$i]['filter_by'] == 'no_of_partners'){

                        $query->orWhere('firm_details.no_of_partners_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                     }
                     elseif($filter[$i]['filter_by'] == 'prefered_gender'){
                        $query->orWhere('firm_requirements.prefered_gender', 'like', '%'.$filter[$i]['filter_value'].'%');
                    }
                    elseif($filter[$i]['filter_by'] == 'qualification'){
                        $query->orWhere('firm_requirements.qualification_needed', 'like', '%'.$filter[$i]['filter_value'].'%');
                    }
                    elseif($filter[$i]['filter_by'] == 'offer_stipend'){
                        $query->orWhere('firm_details.stipened_offered', 'like', '%'.$filter[$i]['filter_value'].'%');
                    }
                    elseif($filter[$i]['filter_by'] == 'location_zone'){
                        $query->orWhere('users.location_zone', 'like', '%'.$filter[$i]['filter_value'].'%');
                    }
                    elseif($filter[$i]['filter_by'] == 'application_status'){
                        if($filter[$i]['filter_value']=='not_applied'){
                            $query->whereNotIn('user_applications.application_status', ['applied','assigned','shortlisted','reject']);
                        }else{
                            $searchterm =$filter[$i]['filter_value'];
                            $relationAttribute = 'user_applications.application_status';
                            $query->orWhereHas('getUserApplicationStatus', function (Builder $query) use ($relationAttribute, $searchterm) {
                                $query->where($relationAttribute, 'like', '%'.$searchterm.'%');
                            });
                        }
                    }

                }
              }
            })

            ->with(['getUserApplicationStatus'=> function ($query) use($uid) {

                $query->select('id','user_id','firm_requirement_id','application_status');
                $query->where('user_id',$uid);

            }])
            ->with('getUsers.getFirmGallery')
            ->where('firm_requirements.looking_role_id',Auth::user()->role_id)
            ->select('firm_requirements.id','users.id as firm_id','users.name as firm_name','users.location_zone','firm_requirements.looking_role_id',
            DB::raw("CASE WHEN firm_requirements.looking_role_id='2' THEN 'Student' ELSE 'Employee' END as looking_role")
            ,DB::raw("CASE WHEN firm_requirements.prefered_gender='0' THEN 'Male' ELSE 'Female' END as gender"),
            DB::raw("CASE WHEN firm_requirements.entry_type='0' THEN 'Direct Entry' ELSE 'IPCC group requirement' END as entry_type"),
            'firm_requirements.maximum_no_of_ipcc_attempts','firm_requirements.area',
            // 'firm_requirements.work_experience','firm_requirements.area',
            'firm_details.exposure_offered','firm_details.stipened_offered','firm_details.no_of_partners_id',
            'number_of_partners.no_of_partners',
            'firm_requirements.minimum_test_mark',
             'firm_requirements.other_requirements',
            'location_zones.location as firm_location_zone',
            'qualifications.name as qualification_needed','firm_requirements.prefered_gender',
            'firm_requirements.approval_status as requirement_status',
            'firm_requirements.user_id as uid'
            ,DB::raw('DATE_FORMAT(firm_requirements.created_at, "%d-%b-%Y") as posted_date'))
            ->withCount('getAllApplications')
            ->orderBy(DB::raw($sort_by1),'ASC');


            $count= $result->count();
            $results=$result->skip($start)->limit($limit)->distinct()->get();

            if($user_id){
                $res1= $result->where('firm_requirements.user_id',$user_id);
            }
            return    response()->json(['message'=>'Requirement List','list' => $results,'count'=>$count,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function viewDetailRequirements(FirmViewRequest $request){
        try {


            $result = ModelsUser::leftjoin('firm_details','firm_details.user_id','users.id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('number_of_partners','number_of_partners.id','firm_details.no_of_partners_id')
            ->leftjoin('firm_requirements','firm_requirements.user_id','users.id')
            ->where('users.id',$request->id)
            ->with('getFirmGallery')
            ->select('users.id','users.name as firm_name','users.mobile','users.email','users.address','users.location_zone',
            'location_zones.location','users.website_id',
            DB::raw("CASE WHEN firm_details.block_status='0' THEN 'Blocked' ELSE 'Active' END as block_status"),
            'number_of_partners.no_of_partners','firm_details.no_of_partners_id',
            'firm_details.contact_person_name','firm_details.exposure_offered',
            'firm_details.stipened_offered','firm_details.about_firm',
            DB::raw('DATE_FORMAT(firm_requirements.created_at, "%d-%b-%Y") as posted_date'),
            'firm_requirements.maximum_no_of_ipcc_attempts','firm_requirements.id as firm_requirement_id',
            DB::raw("CASE WHEN firm_requirements.looking_role_id='2' THEN 'Student' ELSE 'Employee' END as looking_role_id"),
            DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as created_date'))
            ->first();
            return    response()->json(['message'=>'Details of a Firm requiremnt','list' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }

}
