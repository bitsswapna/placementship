<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\EmployeeBasicUpdateRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\MobileOtpResendRequest;
use App\Http\Requests\MobileOtpVerificationRequest;
use App\Http\Requests\MobileVerificationRequest;
use App\Http\Requests\PasswordsetRequest;
use App\Http\Requests\RegisterFullDetailsRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\StudentLoginRequest;
use App\Http\Requests\UserChangePasswordRequest;
use App\Http\Requests\UserResumeUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\VerifyForgotPasswordOtp;
use App\Mail\SendResetPasswordOtp;
use App\Models\PasswordReset;
use App\Models\User;
use App\Models\UserCoursePursuing;
use App\Models\UserDetails;
use App\Models\UserOtp;
use App\Models\UserWorkExperience;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facades\Nexmo;
use Illuminate\Support\Str;
use Vonage\Account\Client;
use Vonage\Client as VonageClient;
use Vonage\Client\Credentials\Basic;

class UserAuthController extends Controller
{
    //
    public function login(StudentLoginRequest $request)
    {
        // try {

            $rules = [
                'mobile' => 'required|mobile',
                'password' => 'required|string',
                'remember_me' => 'boolean'
            ];

            $customMessages = [
                'required' => 'The :attribute field is required.'
            ];
           // $this->validate($request, $rules, $customMessages);

           if($request->type==1){
            $credentials = request(['mobile', 'password']);
            }
            if($request->type==2){
            $credentials = request(['sro_number', 'password']);
            }

            if(!Auth::attempt($credentials))
            {
                return response()->json([
                    'access_token' =>'',
                    'token_type' => '',
                    'expires_at' =>'',
                    'message' => 'Invalid login credentials',
                    'data'=>'',
                    'status'=>false
                ], 200);
            }else{

                $user = $request->user();
                //dd($user->role_id);
                if ($user->accessTokens->count() > 0) {
                    $user->accessTokens()->delete();
                }
                if(($user->role_id==1) || ($user->role_id==4)){
                    return response()->json([
                        'access_token' =>'',
                        'token_type' => '',
                        'expires_at' =>'',
                        'message' => 'Invalid User',
                        'data'=>'',
                        'status'=>false
                    ], 200);
                }else{
                    $tokenResult = $user->createToken('Personal Access Token');
                    $token = $tokenResult->token;
                    if ($request->remember_me)
                        $token->expires_at = Carbon::now()->addWeeks(1);
                    $token->save();
                    return response()->json([
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse(
                            $tokenResult->token->expires_at
                        )->toDateTimeString(),
                        'message'=>'Successfully Logged In',
                        'data'=>$user,
                        'status'=>true
                    ]);
                }
            }
        // }catch (\Exception $exception){
        //     return response()->json(['message'=>$exception->getMessage()],500);
        // }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->delete();
        return response()->json([
            'message' => 'Successfully logged out',
            'status'=>true
        ], 200);
    }

    public function signup(RegisterRequest $request){

        try {
            if($request->input('name')){
                $userDetails = new User();
                $userDetails->name = $request->input('name');
                $userDetails->email = $request->input('email');
                $userDetails->role_id = $request->input('role_id');
                $userDetails->sro_number = $request->input('sro_number');
                $userDetails->dob = $request->input('dob');
                //$userDetails->location_zone = $request->input('location_zone');
                $userDetails->address = $request->input('address');
                $userDetails->gender = $request->input('gender');
                $userDetails->mobile = $request->input('mobile');

                $data=$userDetails->save();

                $otp = $this->generateOTP();
                $storeotp = new UserOtp();
                $storeotp->user_id= $userDetails->id;
                $storeotp->otp=$otp;
                $storeotp->save();
                $message = 'you otp is '.$otp;

                // session(['otp' => $otp]);

                // session(['user_id' => $userDetails->id]);
                $basic  = new Basic(getenv("NEXMO_KEY"), getenv("NEXMO_SECRET"));
                $client = new VonageClient($basic);

                $message = $client->message()->send([
                    'to' => $request->input('mobile'),
                    'from' => 'Placementship',
                    'text' => $message
                ]);

                return response()->json([
                    'message' => 'Successfully Registered',
                    'status'=>true,
                    'mobile'=>$request->input('mobile'),
                    'otp'=>$otp,
                    'user_id'=>$userDetails->id
                ], 200);
            }


        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function generateOTP(){
        $otp = mt_rand(1000,9999);
        return $otp;
    }
    public function mobileVerification(MobileOtpVerificationRequest $request){
        try {
            if($request->input('otp')){
                $getOtp = UserOtp::where('user_id',$request->id)->first();

                if($getOtp!=null){

                    if($request->input('otp') == $getOtp->otp){
                        UserOtp::where(['user_id'=> $request->id])->delete();
                        return response()->json([
                            'message' => 'Success',
                            'user_id'=>$request->id,
                            'mobile'=>$request->input('mobile'),
                            'status'=>true
                        ], 200);
                    }else{
                        return response()->json([
                            'message' => 'Otp Not Valid',
                            'status'=>false
                        ], 200);
                    }
                }else{
                    return response()->json([
                        'message' => 'something went wrong',
                        'status'=>false
                    ], 200);
                }

            }

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function mobileOtpResend(MobileOtpResendRequest $request){
        try {
                UserOtp::where(['user_id'=> $request->id])->delete();
                $otp = $this->generateOTP();
                $storeotp = new UserOtp();
                $storeotp->user_id= $request->id;
                $storeotp->otp=$otp;
                $storeotp->save();
                $message = 'you otp is '.$otp;
                return response()->json([
                    'message' => 'Successfully Sent',
                    'status'=>true,
                    'otp'=>$otp,
                    'mobile'=>$request->input('mobile'),
                    'user_id'=>$request->id
                ], 200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function setupPassword(PasswordsetRequest $request){
        try {
            User::where('id', $request->input('id'))->update(['password' => bcrypt($request->input('password'))]);

            //login

               // $user = User::where('id',$request->input('id'))->first();

                $credentials = request(['mobile', 'password']);
                Auth::attempt($credentials);
                $user = $request->user();

                //dd($user->role_id);

                if ($user->accessTokens->count() > 0) {
                    $user->accessTokens()->delete();
                }

                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();
            //login end
                return response()->json([
                    'message' => 'Successfully Updated',
                    'status'=>true,
                    'user_id'=>$request->input('id'),
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString(),
                    'data'=>$user
                ], 200);

            }catch (\Exception $exception){
                return response()->json(['message'=>$exception->getMessage()],500);
            }

    }
    public function signupFullDetails(RegisterFullDetailsRequest $request){

        try {
             if($request->input('qualification_id')){


                $userDetailss =UserDetails::where('user_id',Auth::user()->id)->first();
                if($userDetailss){

                    $userDetails = UserDetails::where('user_id',Auth::user()->id)->first();
                    $userDetails->user_id = Auth::user()->id;
                    $userDetails->qualification_id = $request->input('qualification_id');

                    $userDetails->tenth_passed_year = $request->input('tenth_passed_year');
                    $userDetails->tenth_percentage = $request->input('tenth_percentage');
                    $userDetails->twelfth_passed_year = $request->input('twelfth_passed_year');
                    $userDetails->twelfth_percentage = $request->input('twelfth_percentage');
                    $userDetails->cpt_number_of_attempts = $request->input('cpt_number_of_attempts');
                    $userDetails->ipcc_number_of_attempts = $request->input('ipcc_number_of_attempts');

                    $userDetails->cpt_percentage = $request->input('cpt_percentage');
                    $userDetails->cpt_passed_year = $request->input('cpt_passed_year');
                    $userDetails->ipcc_percentage = $request->input('ipcc_percentage');
                    $userDetails->ipcc_passed_year = $request->input('ipcc_passed_year');
                    $userDetails->itt_orientation_status = $request->input('itt_orientation_status');

                    $userDetails->degree = $request->input('degree');
                    $userDetails->other_professional_course = $request->input('other_professional_course');
                    $userDetails->achievements = $request->input('achievements');
                    // $userDetails->awards = $request->input('awards');
                    $userDetails->career_objectives = $request->input('career_objectives');

                    $userDetails->extra_curicular_activities = $request->input('extra_curicular_activities');
                    $userDetails->skills = $request->input('skills');
                    $userDetails->other_professional_achievements = $request->input('other_professional_achievements');
                    $userDetails->languages_known = $request->input('languages_known');
                    $userDetails->employment_type_id = $request->input('employment_type_id');
                    $userDetails->ipce_coaching_institute = $request->input('ipce_coaching_institute');
                    $userDetails->type_of_exposure = $request->input('type_of_exposure');


                    $userDetails->save();

                }else{
                    $userDetails = new UserDetails();
                    $userDetails->user_id = Auth::user()->id;
                    $userDetails->qualification_id = $request->input('qualification_id');

                    $userDetails->tenth_passed_year = $request->input('tenth_passed_year');
                    $userDetails->tenth_percentage = $request->input('tenth_percentage');
                    $userDetails->twelfth_passed_year = $request->input('twelfth_passed_year');
                    $userDetails->twelfth_percentage = $request->input('twelfth_percentage');
                    $userDetails->cpt_number_of_attempts = $request->input('cpt_number_of_attempts');
                    $userDetails->ipcc_number_of_attempts = $request->input('ipcc_number_of_attempts');

                    $userDetails->cpt_percentage = $request->input('cpt_percentage');
                    $userDetails->cpt_passed_year = $request->input('cpt_passed_year');
                    $userDetails->ipcc_percentage = $request->input('ipcc_percentage');
                    $userDetails->ipcc_passed_year = $request->input('ipcc_passed_year');
                    $userDetails->itt_orientation_status = $request->input('itt_orientation_status');

                    $userDetails->degree = $request->input('degree');
                    $userDetails->other_professional_course = $request->input('other_professional_course');
                    $userDetails->achievements = $request->input('achievements');
                    // $userDetails->awards = $request->input('awards');
                    $userDetails->career_objectives = $request->input('career_objectives');

                    $userDetails->extra_curicular_activities = $request->input('extra_curicular_activities');
                    $userDetails->skills = $request->input('skills');
                    $userDetails->other_professional_achievements = $request->input('other_professional_achievements');
                    $userDetails->languages_known = $request->input('languages_known');
                    $userDetails->employment_type_id = $request->input('employment_type_id');
                    $userDetails->ipce_coaching_institute = $request->input('ipce_coaching_institute');
                    $userDetails->type_of_exposure = $request->input('type_of_exposure');
                    // $userDetails->selection_status = $request->input('selection_status');


                    $userDetails->save();
                }
            }

            if($request->input('work_experiences')){
                UserWorkExperience::where('user_id',Auth::user()->id)->delete();
                $works=$request->input('work_experiences');
                foreach ($works as $work) {

                        $res=new UserWorkExperience();
                        $res->user_id= $userDetails->user_id;
                        $res->firm_name= $work['firm_name'];
                        $res->no_of_years= $work['no_of_years'];
                        $res->area= $work['area'];
                        $res->save();
                }
            }

            if($request->input('user_course_pursuings')){
                UserCoursePursuing::where('user_id',Auth::user()->id)->delete();
                $usercourse=$request->input('user_course_pursuings');
                foreach ($usercourse as $usercourses) {

                        $res=new UserCoursePursuing();
                        $res->user_id= $userDetails->user_id;
                        $res->course_id= $usercourses['course_id'];
                        $res->save();
                }
            }

            return response()->json([
                'message' => 'Successfully Registered',
                'status'=>true,
                'user_id'=>Auth::user()->id
            ], 200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function uploadResume(UserResumeUpdateRequest $request){
        try {
            if ($files = $request->file('resume')) {
                //store file into document folder

                $filename = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $picture = date('His').$filename;
                $path ='/documents';
                $destinationPath = public_path().'/documents';

                $file = $request->file('resume') ? $files->move($destinationPath, $picture) : null;

                $document = UserDetails::where('user_id',Auth::user()->id)->update([
                    'resume' =>$path.'/'.$picture]);

            }

            return response()->json([
                'message' => 'Successfully uploaded',
                'status'=>true
            ], 200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }


    public function getAuser(Request $request){
        try {
            $result = User::leftjoin('user_exam_details','user_exam_details.user_id','users.id')
            ->leftjoin('user_details','user_details.user_id','users.id')
            ->leftJoin('roles', 'roles.id','users.role_id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('qualifications','qualifications.id','user_details.qualification_id')
                // ->leftjoin('courses','courses.id','user_details.course_id')

            ->with(['getAllWorkExperiences'])
            ->with(['getAllCoursesPursuing.getCourseName'=> function ($query) {
                //$join->on('courses','courses.id','user_course_pursuings.course_id');
                $query->select('courses.id','courses.name as course_name');

            }])
            // ->with(['getAllShortlisted'=> function ($query) {
            //     $query->where('user_applications.application_status', 1);
            // }])
            ->where('users.id',Auth::user()->id)
            ->select('users.id','roles.name as role','users.name',DB::raw('DATE_FORMAT(users.dob, "%d-%b-%Y") as dob'),'users.email','users.sro_number','users.location_zone','location_zones.location',
            DB::raw("CASE WHEN users.gender='0' THEN 'Male' WHEN users.gender='1' THEN 'Female' ELSE 'Others' END as gender"),'user_exam_details.total_score',
            DB::raw("CASE WHEN user_details.selection_status='0' THEN 'Not appointed' ELSE 'appointed' END as appointed_status"),
            'users.address','user_details.college_name','user_details.tenth_passed_year','user_details.tenth_percentage','user_details.twelfth_passed_year',
            'user_details.twelfth_percentage','user_details.cpt_number_of_attempts','user_details.ipcc_number_of_attempts','user_details.degree','user_details.other_professional_course',
            'user_details.achievements','user_details.career_objectives','user_details.extra_curicular_activities','user_details.skills',
            'user_details.other_professional_achievements','user_details.resume',
            'user_details.cpt_percentage','user_details.cpt_passed_year',
            'user_details.ipcc_percentage','user_details.ipcc_passed_year',
            'user_details.languages_known','user_details.employment_type_id','user_details.ipce_coaching_institute','user_details.type_of_exposure',
            DB::raw("CASE WHEN user_details.itt_orientation_status='0' THEN 'incompleted' ELSE 'completed' END as itt_orientation_status")
            ,'user_details.qualification_id','qualifications.name as qualification'
            ,'users.mobile',DB::raw("CASE WHEN users.profile_status='0' THEN 'Registered' WHEN users.profile_status='1' THEN 'Completed' WHEN users.profile_status='2' THEN 'Attended Exam' WHEN users.profile_status='3' THEN 'Resume Uploaded' WHEN users.profile_status='4' THEN 'Password Set' ELSE 'Mobile Verified' END as profile_status")
            ,'user_exam_details.total_score',
            DB::raw('DATE_FORMAT(users.created_at, "%d-%b-%Y") as joined_date'))
            ->withCount('getAllApplications')
            ->withCount(['getAllShortlisted'=> function ($query) {
                $query->where('user_applications.application_status', 1);
            }])
            ->get();
            return    response()->json(['message'=>'Details of a Student/Employee','details' => $result,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function editBasicsUser(UserUpdateRequest $request){
        try {

            $userDetails = User::find(Auth::user()->id);
            $userDetails->name = $request->input('name');
            $userDetails->email = $request->input('email');
            $userDetails->sro_number = $request->input('sro_number');
            $userDetails->dob = $request->input('dob');
           // $userDetails->location_zone = $request->input('location_zone');
            $userDetails->address = $request->input('address');
            $userDetails->gender = $request->input('gender');
            $userDetails->mobile = $request->input('mobile');

            $userDetails->save();


            $userDetailss =UserDetails::where('user_id',Auth::user()->id)->first();

            $userDetailss->qualification_id = $request->input('qualification_id');

            $userDetailss->tenth_passed_year = $request->input('tenth_passed_year');
            $userDetailss->tenth_percentage = $request->input('tenth_percentage');
            $userDetailss->twelfth_passed_year = $request->input('twelfth_passed_year');
            $userDetailss->twelfth_percentage = $request->input('twelfth_percentage');
            $userDetailss->cpt_number_of_attempts = $request->input('cpt_number_of_attempts');
            $userDetailss->ipcc_number_of_attempts = $request->input('ipcc_number_of_attempts');

            $userDetailss->cpt_percentage = $request->input('cpt_percentage');
            $userDetailss->cpt_passed_year = $request->input('cpt_passed_year');
            $userDetailss->ipcc_percentage = $request->input('ipcc_percentage');
            $userDetailss->ipcc_passed_year = $request->input('ipcc_passed_year');
            $userDetailss->itt_orientation_status = $request->input('itt_orientation_status');

            $userDetailss->degree = $request->input('degree');
            $userDetailss->other_professional_course = $request->input('other_professional_course');
            $userDetailss->achievements = $request->input('achievements');
            // $userDetailss->awards = $request->input('awards');
            $userDetailss->career_objectives = $request->input('career_objectives');

            $userDetailss->extra_curicular_activities = $request->input('extra_curicular_activities');
            $userDetailss->skills = $request->input('skills');
            $userDetailss->other_professional_achievements = $request->input('other_professional_achievements');
            // $userDetailss->selection_status = $request->input('selection_status');
            $userDetailss->languages_known = $request->input('languages_known');
            $userDetailss->employment_type_id = $request->input('employment_type_id');
            $userDetailss->ipce_coaching_institute = $request->input('ipce_coaching_institute');
            $userDetailss->type_of_exposure = $request->input('type_of_exposure');

            $userDetailss->save();

            if($request->input('work_experiences')){
                $works=$request->input('work_experiences');
                UserWorkExperience::where('user_id',$userDetails->id)->delete();
                foreach ($works as $work) {
                        $res=new UserWorkExperience();
                        $res->user_id= $userDetails->id;
                        $res->firm_name= $work['firm_name'];
                        $res->no_of_years= $work['no_of_years'];
                        $res->area= $work['area'];
                        $res->save();
                }
            }
            if($request->input('courses')){
                $courses=$request->input('courses');
                UserCoursePursuing::where('user_id',$userDetails->id)->delete();
                foreach ($courses as $course) {

                        $res=new UserCoursePursuing();
                        $res->user_id= $userDetails->id;
                        $res->course_id= $course['course_id'];
                        $res->save();

                }

            }
            if ($files = $request->file('resume')) {
                //store file into document folder

                //$file = uploadAnyFile('documents', $request->file('resume'));

                $file = $request->file('resume') ? $request->file('resume')->store('public/documents') : null;

                $document = UserDetails::where('user_id',$request->id)->update([
                    'resume' =>$file]);
            }


            return    response()->json(['message'=>'Successfully Updated','status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function forgotPassword(ForgotPasswordRequest $request){
        try{


            $email = $request->input('email');
            $user = User::where('email', $email)->first();

            if ($user) {
                $name = $user->name;
                $email = $user->email;

                $token = Str::random(10);

                $user->remember_token = $token;
                $user->save();

                $password_resets = new PasswordReset();
                $password_resets->email = $email;
                $password_resets->token = $token;
                $password_resets->save();

    //            \Mail::to($email)->send(new SendResetPasswordMailable($name,$token));

                if ($email) {
                    Mail::to($email)->send(new SendResetPasswordOtp($name,$token));
                   // Mail::to($email)->queue(new SendResetPasswordMailable($name,$token));
                }

                return    response()->json(['message'=>'An OTP is send to your email','email'=>$request->input('email'),'user'=>$user,'otp'=>$token],200);

            }else {
                return    response()->json(['message'=>'Your Account Does not Exist','status'=>false],200);

            }
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }

    }
    public function verifyOtp(VerifyForgotPasswordOtp $request){
        try {
                $verify = User::where('email',$request->input('email'))->first();
                if($verify->remember_token==$request->input('token')){
                    return    response()->json(['message'=>'success','p_token'=>$request->input('token'),'email'=> $verify->email,'status'=>true]);
                }else{
                    return    response()->json(['message'=>'Token Expired','status'=>false],422);
                }

        }
        catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }


    }
    public function changePassword(UserChangePasswordRequest $request){
        try {
            $verify = User::where('email',$request->input('email'))->first();
            if (!(Hash::check($request->old_password, $verify->password))) {
            //if ((bcrypt($request->input('old_password')))!=( Auth::user()->password)) {

                //dd(bcrypt('admin'));
                return response()->json([
                    'message' => 'Your old password does not matches with the password. ',
                    'status'=>false
                ], 200);
            }
            if ((Hash::check($request->new_password, $verify->password))) {
            //if ((bcrypt($request->input('new_password')))==( Auth::user()->password)) {
                return response()->json([
                    'message' => 'New Password cannot be same as your current password. ',
                    'status'=>false
                ], 200);
            }

            $user = User::find($verify->id);
            $user->password = bcrypt($request->input('new_password'));
            $user->save();
            return response()->json([
                'message' => 'Password changed Successfully.',
                'status'=>false
            ], 200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }


}
