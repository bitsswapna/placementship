<?php

namespace App\Http\Controllers;

use App\Models\User as ModelsUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

class AdminAuthController extends Controller
{
    //
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    // public function __construct()
    // {

    // }
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new ModelsUser([
            'name' => $request->name,
            'role_id' => 1,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'sro_number' => 0,
            'location_zone' => '',
            'address' => '',
            'mobile'=>''
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!',
            'status'=>true
        ], 200);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        // try {

            $rules = [
                'email' => 'required|string|email',
                'password' => 'required|string',
                'remember_me' => 'boolean'
            ];

            $customMessages = [
                'required' => 'The :attribute field is required.'
            ];
            $this->validate($request, $rules, $customMessages);

            // $request->validate([
            //     'email' => 'required|string|email',
            //     'password' => 'required|string',
            //     'remember_me' => 'boolean'
            // ]);
            $credentials = request(['email', 'password']);
            if(!Auth::attempt($credentials))
            {
                return response()->json([
                    'access_token' =>'',
                    'token_type' => '',
                    'expires_at' =>'',
                    'message' => 'Invalid login credentials',
                    'data'=>'',
                    'status'=>false
                ], 200);
            }else{
                $user = $request->user();
                //dd($user->role_id);
                if ($user->accessTokens->count() > 0) {
                    $user->accessTokens()->delete();
                }
                if($user->role_id!=1){
                    return response()->json([
                        'access_token' =>'',
                        'token_type' => '',
                        'expires_at' =>'',
                        'message' => 'Invalid User',
                        'data'=>'',
                        'status'=>false
                    ], 200);
                }else{
                    $tokenResult = $user->createToken('Personal Access Token');
                    $token = $tokenResult->token;
                    if ($request->remember_me)
                        $token->expires_at = Carbon::now()->addWeeks(1);
                    $token->save();
                    return response()->json([
                        'access_token' => $tokenResult->accessToken,
                        'token_type' => 'Bearer',
                        'expires_at' => Carbon::parse(
                            $tokenResult->token->expires_at
                        )->toDateTimeString(),
                        'message'=>'Successfully Logged In',
                        'data'=>$user,
                        'status'=>true
                    ]);
                }
            }
        // }catch (\Exception $exception){
        //     return response()->json(['message'=>$exception->getMessage()],500);
        // }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->delete();
        return response()->json([
            'message' => 'Successfully logged out',
            'status'=>true
        ], 200);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {

        return response()->json($request->user());
    }
    public function changePassword(ChangePasswordRequest $request){
        try {
            if (!(Hash::check($request->old_password, Auth::user()->password))) {
            //if ((bcrypt($request->input('old_password')))!=( Auth::user()->password)) {

                //dd(bcrypt('admin'));
                return response()->json([
                    'message' => 'Your old password does not matches with the password. ',
                    'status'=>false
                ], 200);
            }
            if ((Hash::check($request->new_password, Auth::user()->password))) {
            //if ((bcrypt($request->input('new_password')))==( Auth::user()->password)) {
                return response()->json([
                    'message' => 'New Password cannot be same as your current password. ',
                    'status'=>false
                ], 200);
            }

            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->input('new_password'));
            $user->save();
            return response()->json([
                'message' => 'Password changed Successfully.',
                'status'=>false
            ], 200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
}
