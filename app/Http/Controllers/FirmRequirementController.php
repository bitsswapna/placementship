<?php

namespace App\Http\Controllers;

use App\Http\Requests\AllApplicationsRequest;
use App\Http\Requests\ChangeStatusRequirementRequest;
use App\Http\Requests\RequirementListRequest;
use App\Models\FirmRequirement;
use App\Models\User;
use App\Models\UserApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FirmRequirementController extends Controller
{
    //

    public function getAllRequirements(RequirementListRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;
            $type=$request->type;
            $firm_id=$request->firm_id;


            $sort_by=$request->sort_by;
            $filter=$request->filter;
            //$filter_value=$request->filter_value;
            $user_id =$request->user_id;

            if($sort_by!=NULL){
                if($sort_by == "created_at")
                    $sort_by1='firm_requirements.created_at';

            }else{
                $sort_by1='firm_requirements.id';
            }



            $result = FirmRequirement::leftjoin('users','users.id','firm_requirements.user_id')
            ->leftjoin('location_zones','users.location_zone','location_zones.id')
            ->leftjoin('qualifications','qualifications.id','firm_requirements.qualification_needed')
            ->where(function($query) use ($filter) {
                if($filter!=NULL){
                for($i=0;$i<count($filter);$i++){
                // dd($filter);
                // if(($filter[$i]['filter_by'] == 'maximum_no_of_ipcc_attempts') || ($filter[$i]['filter_by'] == 'looking_role_id')  || ($filter[$i]['filter_by'] == 'prefered_gender')){
                //     //dd('hi');
                //     $query->where('firm_requirements.maximum_no_of_ipcc_attempts', 'like', '%'.$filter[$i]['filter_value'].'%');
                //     $query->orWhere('firm_requirements.looking_role_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                //     $query->orWhere('firm_requirements.prefered_gender', 'like', '%'.$filter[$i]['filter_value'].'%');
                // }
                // elseif(($filter[$i]['filter_by'] == 'maximum_no_of_ipcc_attempts') || ($filter[$i]['filter_by'] == 'looking_role_id') ){
                //     //dd('hi');
                //     $query->where('firm_requirements.maximum_no_of_ipcc_attempts', 'like', '%'.$filter[$i]['filter_value'].'%');
                //     $query->where('firm_requirements.looking_role_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                // }

                    if($filter[$i]['filter_by'] == 'maximum_no_of_ipcc_attempts'){
                        $query->where('firm_requirements.maximum_no_of_ipcc_attempts', 'like', '%'.$filter[$i]['filter_value'].'%');
                     }
                     elseif($filter[$i]['filter_by'] == 'looking_role_id'){

                        $query->orWhere('firm_requirements.looking_role_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                     }
                     elseif($filter[$i]['filter_by'] == 'prefered_gender'){
                        $query->orWhere('firm_requirements.prefered_gender', 'like', '%'.$filter[$i]['filter_value'].'%');
                    }

                    elseif($filter[$i]['filter_by'] == 'qualification'){
                        $query->orWhere('firm_requirements.qualification_needed', 'like', '%'.$filter[$i]['filter_value'].'%');
                    }

                }
               }
            })
            ->where(function($query) use ($type) {
                if($type=='all'){
                }else{
                    $query->where('firm_requirements.approval_status',$type);
                }
            })
            ->where(function($query) use ($firm_id) {
                if($firm_id==''){
                }else{
                    $query->where('firm_requirements.user_id',$firm_id);
                }
            })
            ->select('firm_requirements.id','users.id as firm_id','users.name as firm_name','firm_requirements.looking_role_id',
            DB::raw("CASE WHEN firm_requirements.looking_role_id='2' THEN 'Student' ELSE 'Employee' END as looking_role")
            ,DB::raw("CASE WHEN firm_requirements.prefered_gender='0' THEN 'Male' ELSE 'Female' END as gender"),
            'firm_requirements.maximum_no_of_ipcc_attempts',
            // 'firm_requirements.work_experience','firm_requirements.area',
            'firm_requirements.minimum_test_mark',
            // 'firm_requirements.other_requirements',
            //'location_zones.location as firm_location_zone',
            'qualifications.name as qualification_needed','firm_requirements.prefered_gender',
            'firm_requirements.approval_status as requirement_status'
            ,DB::raw('DATE_FORMAT(firm_requirements.created_at, "%d-%b-%Y") as posted_date'))
            ->withCount('getAllApplications')
            ->orderBy(DB::raw($sort_by1),'ASC');


            $count= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            if($user_id){
                $res1= $result->where('firm_requirements.user_id',$user_id);
            }
            return    response()->json(['message'=>'Requirement List','list' => $results,'count'=>$count,'status'=>true],200);

        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }
    }
    public function changeStatus(ChangeStatusRequirementRequest $request){
        try {
            $result = FirmRequirement::where('id',$request->id)->update([
                'approval_status' =>$request->status]);
            return    response()->json(['message'=>'Status Updated Successfuly','status'=>true],200);
        }catch (\Exception $exception){
            return response()->json(['message'=>$exception->getMessage()],500);
        }

    }
    public function getAllApplications(AllApplicationsRequest $request){
        try {

            $limit=$request->limit;
            $start=$request->start;
            $userid=$request->userid;
            $status=$request->status;


            $id = $request->id;
            $firm_id = $request->firm_id;
            $sort=$request->sort;
            $filter=$request->filter;
            //$filter_value=$request->filter_value;'

            $firm = User::where('id',$firm_id)->select('id as firm_id','name as firm_name')->get();

            if(!empty($sort)){
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='user_applications.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "created_at" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='user_applications.created_at';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "score" && $sort[0]['sort_order'] == "ASC")
                    $sort_by1='user_exam_details.total_score';
                    $sort_order1=$sort[0]['sort_order'];
                if($sort[0]['sort_by'] == "score" && $sort[0]['sort_order'] == "DESC")
                    $sort_by1='user_exam_details.total_score';
                    $sort_order1=$sort[0]['sort_order'];
            }else{
                $sort_by1='user_applications.id';
                $sort_order1="DESC";
            }


            $result = UserApplication::leftjoin('users','users.id','user_applications.user_id')
            ->leftjoin('user_details','user_details.user_id','user_applications.user_id')
            ->leftjoin('user_exam_details','user_exam_details.user_id','user_applications.user_id')
            ->leftjoin('qualifications','qualifications.id','user_details.qualification_id')
            ->where(function($query) use ($filter) {
                if($filter!=NULL){
                    for($i=0;$i<count($filter);$i++){
                        if($filter[$i]['filter_by'] == 'ipcc_number_of_attempts'){
                            $query->where('user_details.ipcc_number_of_attempts', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }
                        if($filter[$i]['filter_by'] == 'application_status'){
                            $query->orWhere('user_applications.application_status', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }
                        if($filter[$i]['filter_by'] == 'gender'){
                            $query->orWhere('users.gender', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }
                        if($filter[$i]['filter_by'] == 'qualification'){
                            $query->orWhere('user_details.qualification_id', 'like', '%'.$filter[$i]['filter_value'].'%');
                        }
                    }
                }
            })
            ->with(['getWorkExperience'=> function ($query) {
                $query->select('id','no_of_years');

            }])
            ->where('user_applications.firm_requirement_id',$id)
            ->where(function($query) use ($userid,$status) {
                if($userid==''){
                }else{
                    if($status=='')
                    {
                    $query->where('user_applications.user_id',$userid);
                    }

                    else
                    {
                        //dd('hi');
                    $query->where(['user_applications.user_id'=>$userid,
                    'user_applications.application_status'=>$status
                    ]);
                    }


                }
            })
            ->select('user_applications.id','users.id as user_id','users.name as user_applied','user_exam_details.total_score as user_mark',
            DB::raw("CASE WHEN users.role_id='2' THEN 'Student' ELSE 'Employee' END as type")
            ,'users.gender',DB::raw("CASE WHEN users.gender='0' THEN 'Male' ELSE 'Female' END as user_gender"),
            'user_details.ipcc_number_of_attempts',
            'qualifications.id as qualification_id',
            'qualifications.name as qualification','users.gender',
            'user_applications.application_status as application_status'
            ,DB::raw('DATE_FORMAT(user_applications.created_at, "%d-%b-%Y") as applied_date'))
            ->orderBy($sort_by1,$sort_order1);


            $count= $result->count();
            $results=$result->skip($start)->limit($limit)->get();

            return    response()->json(['message'=>'Firm Application List','list' => $results,'count'=>$count,'firm_name'=>$firm,'status'=>true],200);
        }
        catch (\Exception $exception){
        }

    }
    public function assignApplications(Request $request){
        try {
            $result = UserApplication::where('id',$request->id)->update([
                'application_status' =>'assigned']);
            return    response()->json(['message'=>'Assigned Successfully','status'=>true],200);
        }
        catch (\Exception $exception){
        }

    }
}
