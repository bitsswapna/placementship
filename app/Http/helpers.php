<?php

use Illuminate\Support\Facades\Auth;

  function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->role_id);
    foreach ($permissions as $key => $value) {
      if($value == $userAccess){
        return true;
      }
    }
    return false;
  }


  function getMyPermission($id)
  {
    switch ($id) {
      case 1:
        return 'admin';
        break;
      case 2:
        return 'student';
        break;
      case 3:
        return 'employee';
        break;
      case 4:
        return 'firm';
        break;
    }
  }


?>
