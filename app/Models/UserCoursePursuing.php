<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCoursePursuing extends Model
{
    use HasFactory,SoftDeletes;
    public function getCourseName(){
        return $this->hasOne('App\Models\Course','id','course_id');
    }
}
