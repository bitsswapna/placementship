<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserApplication extends Model
{
    use HasFactory,SoftDeletes;
    public function getFirmRequirement(){
        return $this->hasOne('App\Models\FirmRequirement','id','firm_requirement_id');
    }
    public function getWorkExperience(){
        return $this->hasMany('App\Models\UserWorkExperience','user_id','user_id');
    }

}
