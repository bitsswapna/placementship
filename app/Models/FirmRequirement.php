<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FirmRequirement extends Model
{
    use HasFactory,SoftDeletes;
    public function getFirm(){
        return $this->hasOne('App\Models\User','id','user_id');
    }
    public function getAllApplications(){
        return $this->hasMany('App\Models\UserApplication','firm_requirement_id','id');
    }
    public function getUserApplicationStatus(){
        return $this->hasOne('App\Models\UserApplication','firm_requirement_id','id');
    }
    public function getUsers(){
        return $this->hasOne('App\Models\User','id','user_id')->select('id','name');
    }

}
