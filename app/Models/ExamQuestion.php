<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamQuestion extends Model
{
    use HasFactory,SoftDeletes;
    public function getAnswers(){
        return $this->hasMany('App\Models\ExamAnswer','question_id','id');
    }
    public function getCorrectOption(){
        return $this->hasOne('App\Models\ExamAnswer','question_id','id')->where('correct_option',1);
    }
}
