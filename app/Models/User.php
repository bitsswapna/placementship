<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function accessTokens()
    {
        return $this->hasMany(OauthAccessToken::class);
    }
    public function getAllApplications(){
        return $this->hasMany('App\Models\UserApplication','user_id','id');
    }
    public function getAllShortlisted(){
        return $this->hasMany('App\Models\UserApplication','user_id','id');
    }
    public function getAllRequirements(){
        return $this->hasMany('App\Models\FirmRequirement','user_id','id');
    }
    public function getAllCoursesPursuing(){
        return $this->hasMany('App\Models\UserCoursePursuing','user_id','id')->select('course_id','user_id');
    }
    public function getAllWorkExperiences(){
        return $this->hasMany('App\Models\UserWorkExperience','user_id','id');
    }
    public function getFirmGallery(){
        return $this->hasMany('App\Models\FirmGallery','user_id','id')->select('user_id','id','image_path');
    }
    public function location()
    {
        return $this->belongsTo('App\Models\LocationZone','location_zone','id');
    }
    public function preferedLocation()
    {
        return $this->belongsTo('App\Models\LocationZone','prefered_location_to_work','id');
    }
}
